# Assumptions

1. Satellite IDs in the input file are integers and formatted without any separators.
1. Any satellite ID appearing in the input file is a valid satellite ID, and should be tracked accordingly. This assumption means that the application does not require that the input file contain data for only two satellites.
1. Records in the input file are in chronological order, with the earliest records coming first.
1. Out-of-range readings that appear in multiple intervals should generate multiple alerts. For example, if a battery value under the red low limit occurs at 12:01, 12:03, 12:04, and 12:06 there would be two alerts, one timestamped 12:01 and one timestamped 12:03.
1. The input format is stable. That is, a change to the input format is infrequent enough that rewriting code to
  handle the new format is acceptable. The code that cares about the input format is contained in the input package,
  although the Reading class in the data package would need to change if the set of fields changed (as would much of the application).
1. The output format is stable. That is, a change to the output format is infrequent enough that rewriting code to
  handle the new format is acceptable. At present all output formatting is handled by a third-party library based
  on the names of properties on the Alert class, so changes to the output format could require substantial extra
  code. Nothing in the application itself depends on the output format, however, so changing the output format
  should have no impact outside of the output formatting itself.
1. The set of limits to check is stable. That is, a change to the set of limits being checked is infrequent
  enough that modifying code to handle the new checks is acceptable. This assumption gave me the most
  heartache with the exercise, as there are some clear paths to detecting and handling other violation conditions.
  To avoid overengineering, however, I adopted this assumption. I did make some moves towards making it easy to add new conditions; only App.java should need to be modified to add new checks. 
1. The number of out-of-range conditions necessary within a given period to generate an alert  is always 3. This can be changed, but right now it's hard-coded in App.java. If a configuration mechanism were added it would be trivial to set this value from configuration.
1. Data will always be provided in a file named on the command line. I have assumed that there's no need to read from stdin for this application.
1. The interval in which three out-of-range conditions must occur will not change frequently (currently five minutes). The code can be modified to change this interval, but it would not be as easy to make this configurable as it would be to make the number of out-of-range values that prompt an alert.
1. The input file will not be to large to be held in half of the available memory. The application does not read the entire file into memory at once, but this assumption is a useful constraint in the worst case.
1. The input file format is stable. Changing the input file format would only require modifying the classes in the input package, however.
1. The timestamps in the input file are formatted as suggested by the test data: YYYYMMdd HH:mm:ss.SSS
1. The red and yellow high and low limits will all have the same format.
1. The limits will be numeric with no missing data (NaN, blank, etc.)
1. The limits will be formatted as decimal numbers.
1. Quirks of floating-point numbers will not be significant for values in the input file or for comparison between them.
1. The raw value will have the same format as the red and yellow high and low limits (decimal numbers, no missing data, etc.)
1. All fields in the input file will be present (not blank) in all records in the file.
1. The output format is not likely to change. Right now output formatting is handled by a single line of code, and nothing else depends on that line, so changing the output format should not impact the rest of the application. Still, new code for the new output format would need to be written if translating JSON to the new format were not feasible.
1. The format for timestamps in the output should be YYYY-MM-ddTHH:mm:ss.SSSZ as implied by the sample output data.
1. The component names in the output will be the same as the component names in the input.
1. Only BATT and TSTAT component data needs to be handled; readings concerning any other component can be ignored.