package com.pshirley.pagingmissioncontrol.input;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import com.pshirley.pagingmissioncontrol.data.Reading;

/**
 * Parser for a single line of readings. This class parses a single line of
 * input from a set of readings, where the input is ASCII text with
 * pipe-delimited records. The format is:
 * 
 * <pre>
 * &lt;timestamp&gt;|&lt;satellite-id&gt;|&lt;red-high-limit&gt;|&lt;yellow-high-limit&gt;|&lt;yellow-low-limit&gt;|&lt;red-low-limit&gt;|&lt;raw-value&gt;|&lt;component&gt;
 * </pre>
 * 
 * <p>
 * This class is not intended to be instantiated, and provides only a single
 * static utility method, {@link #readingFrom(String)}. This class is thread-safe.
 * </p>
 * <p>
 * Implementation note: This class contains all of the magic values for parsing
 * a single input line. If the input format changes, this class will need to
 * change with it. {@link Reading} may also need to change if the new format
 * includes data not accommodated by the current implementation of Reading.
 * </p>
 * 
 * @author peter
 */
public class ReadingParser {
	/**
	 * Create a Reading from a single input line in the format described by the
	 * class documentation.
	 * 
	 * @param inputLine the input line from which a Reading should be created. This
	 *                  parameter must not be null.
	 * 
	 * @return a Reading created from the given input line
	 * 
	 * @throws IllegalArgumentException if the given input line does not have the
	 *                                  required number of fields
	 */
	public static Reading readingFrom(String inputLine) {
		return INSTANCE.parse(inputLine);
	}

	private final static ReadingParser INSTANCE = new ReadingParser();

	/*
	 * Field indexes.
	 */
	private static final int FIELD_TIMESTAMP = 0;
	private static final int FIELD_SATELLITE_ID = 1;
	private static final int FIELD_RED_HIGH_LIMIT = 2;
	private static final int FIELD_YELLOW_HIGH_LIMIT = 3;
	private static final int FIELD_YELLOW_LOW_LIMIT = 4;
	private static final int FIELD_RED_LOW_LIMIT = 5;
	private static final int FIELD_RAW_VALUE = 6;
	private static final int FIELD_COMPONENT = 7;

	/*
	 * Other formatting details
	 */
	private static final String INPUT_TIMESTAMP_PATTERN = "yyyyMMdd HH:mm:ss.SSS";
	private static final DateTimeFormatter timestampFormatter = DateTimeFormatter.ofPattern(INPUT_TIMESTAMP_PATTERN);
	private static final String FIELD_SEPARATOR_REGEX = "\\|";
	private static final Integer EXPECTED_FIELD_COUNT = Integer.valueOf(8);
	private static final String FORCED_TIMEZONE = "Z"; // Zulu time specified in output format

	/**
	 * Parse a record in the format described in the class documentation.
	 * 
	 * @param record the input record to be parsed. This parameter must not be null.
	 * 
	 * @return a Reading parsed from the given record
	 * 
	 * @throws IllegalArgumentException if the given record does not have the
	 *                                  required number of fields
	 */
	private Reading parse(String record) {
		String[] fields = record.split(FIELD_SEPARATOR_REGEX);

		if (fields.length != EXPECTED_FIELD_COUNT) {
			throw new IllegalArgumentException("Parsed " + fields.length + " fields from record \"" + record
					+ "\", expected " + EXPECTED_FIELD_COUNT);
		}

		return makeReading(fields);
	}

	/**
	 * Create a Reading object from an array of field values.
	 * 
	 * @param fields the field values to use when creating a Reading. This parameter
	 *               must not be null, must have the correct number of elements, and
	 *               each element must contain a valid value for the corresponding field.
	 * 
	 * @return a new Reading object populated with values from fields
	 */
	private Reading makeReading(String[] fields) {
		// @formatter:off
		return new Reading(
				parseTimestamp (fields[FIELD_TIMESTAMP]),
				Integer.valueOf(fields[FIELD_SATELLITE_ID]),
				Double.valueOf (fields[FIELD_RED_HIGH_LIMIT]),
				Double.valueOf (fields[FIELD_YELLOW_HIGH_LIMIT]),
				Double.valueOf (fields[FIELD_YELLOW_LOW_LIMIT]),
				Double.valueOf (fields[FIELD_RED_LOW_LIMIT]),
				Double.valueOf (fields[FIELD_RAW_VALUE]),
				                fields[FIELD_COMPONENT]);
		// @formatter:on
	}

	/**
	 * Parse a timestamp in the format specified by {@link INPUT_TIMESTAMP_PATTERN}
	 * into a ZonedDateTime.
	 * 
	 * @param timestamp the timestamp to parse. This parameter must not be null.
	 * 
	 * @return the parsed timestamp
	 */
	private ZonedDateTime parseTimestamp(String timestamp) {
		final LocalDateTime localDateTime = LocalDateTime.parse(timestamp, timestampFormatter);
		return ZonedDateTime.of(localDateTime, ZoneId.of(FORCED_TIMEZONE));
	}

	/**
	 * Constructor private to prevent instantiation.
	 */
	private ReadingParser() {
		// No instantiation, please
	}
}
