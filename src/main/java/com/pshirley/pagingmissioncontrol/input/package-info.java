/**
 * This package provides classes to generate Readings from input data.
 * 
 * @author peter
 */
package com.pshirley.pagingmissioncontrol.input;