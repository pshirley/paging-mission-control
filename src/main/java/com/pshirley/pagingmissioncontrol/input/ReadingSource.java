package com.pshirley.pagingmissioncontrol.input;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;
import java.util.NoSuchElementException;

import com.pshirley.pagingmissioncontrol.data.Reading;

/**
 * An iterable source for Readings.
 * <p>
 * This class is not thread-safe, and is not intended to be shared between
 * threads.
 * </p>
 * 
 * @author peter
 */
public class ReadingSource implements Iterable<Reading> {
	final BufferedReader input;

	/**
	 * Return a ReadingSource wrapping the given Reader.
	 * 
	 * @param input the Reader providing input to the ReadingSource. This parameter
	 *              must not be null.
	 * 
	 * @return a ReadingSource wrapping the given reader
	 */
	public static ReadingSource from(Reader input) {
		return new ReadingSource(input);
	}

	/**
	 * Create a ReadingSource wrapping the given reader.
	 * 
	 * @param input the Reader providing input to this ReadingSource. This parameter
	 *              must not be null.
	 */
	private ReadingSource(Reader input) {
		this.input = new BufferedReader(input);
	}

	@Override
	public Iterator<Reading> iterator() {
		return new ReadingIterator();
	}

	/**
	 * Iterator over the readings in this reading source.
	 * 
	 * @author peter
	 */
	private class ReadingIterator implements Iterator<Reading> {
		private Reading cachedReading = null;

		@Override
		public boolean hasNext() {
			if (cachedReading == null) {
				cachedReading = nextReading();
			}

			return cachedReading != null;
		}

		@Override
		public Reading next() {
			final Reading reading = cachedReading == null ? nextReading() : cachedReading;
			cachedReading = null;

			if (reading == null) {
				throw new NoSuchElementException();
			}

			return reading;
		}

	}

	/**
	 * Retrieve the next reading. Returns null if there are no more readings.
	 * 
	 * @return the next reading, or null if there are no more readings.
	 */
	private Reading nextReading() {
		Reading reading = null;
		try {
			final String line = input.readLine();
			if (line != null && !line.isEmpty()) {
				reading = ReadingParser.readingFrom(line);
			}
		} catch (IOException e) {
			System.err.println("Caught exception trying to read an input line: " + e);
			return null;
		}
		return reading;
	}
}
