package com.pshirley.pagingmissioncontrol.data;

import java.time.ZonedDateTime;

/**
 * A single reading for a satellite. Readings are immutable value objects.
 * 
 * @author peter
 */
public class Reading {
	private final ZonedDateTime timestamp;
	private final Integer satelliteId;
	private final Double redHighLimit;
	private final Double redLowLimit;
	private final Double yellowHighLimit;
	private final Double yellowLowLimit;
	private final Double rawValue;
	private final String component;

	/**
	 * Creates a reading from the specified values. All of the arguments to this
	 * constructor must be non-null.
	 * 
	 * @param timestamp       the timestamp of the reading
	 * @param satelliteId     the ID of the satellite the reading is from
	 * @param redHighLimit    the red high limit of this reading
	 * @param yellowHighLimit the yellow high limit of this reading
	 * @param yellowLowLimit  the yellow low limit of this reading
	 * @param redLowLimit     the red low limit of this reading
	 * @param rawValue        the raw value for this reading
	 * @param component       the component from which this reading was taken
	 */
	// @formatter:off
	public Reading(
			ZonedDateTime timestamp,
			Integer satelliteId, 
			Double redHighLimit,
			Double yellowHighLimit, 
			Double yellowLowLimit,
			Double redLowLimit, 
			Double rawValue,
			String component) {
	// @formatter:on
		this.timestamp = timestamp;
		this.satelliteId = satelliteId;
		this.redHighLimit = redHighLimit;
		this.yellowHighLimit = yellowHighLimit;
		this.yellowLowLimit = yellowLowLimit;
		this.redLowLimit = redLowLimit;
		this.rawValue = rawValue;
		this.component = component;
	}

	public ZonedDateTime getTimestamp() {
		return timestamp;
	}

	public Integer getSatelliteId() {
		return satelliteId;
	}

	public Double getRedHighLimit() {
		return redHighLimit;
	}

	public Double getYellowHighLimit() {
		return yellowHighLimit;
	}

	public Double getYellowLowLimit() {
		return yellowLowLimit;
	}

	public Double getRedLowLimit() {
		return redLowLimit;
	}

	public Double getRawValue() {
		return rawValue;
	}

	public String getComponent() {
		return component;
	}
}
