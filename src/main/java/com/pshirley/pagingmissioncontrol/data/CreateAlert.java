package com.pshirley.pagingmissioncontrol.data;

/**
 * Utility class to create an alert from a reading.
 * 
 * @author peter
 */
public class CreateAlert {
	/**
	 * Create an alert from a reading and a string describing the alert's severity.
	 * 
	 * @param reading the Reading to base the alert on. This parameter must not be null.
	 * @param severity the severity to assign to the alert. This parameter must not be null.
	 * 
	 * @return a new alert based on the given reading, with the given severity
	 */
	public static Alert from(Reading reading, String severity) {
		return new Alert(reading.getSatelliteId(), reading.getComponent(), severity, reading.getTimestamp());
	}
}
