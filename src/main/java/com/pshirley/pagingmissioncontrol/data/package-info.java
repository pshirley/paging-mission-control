/**
 * This package provides immutable data objects used by the application and utility classes for operating on them.
 * 
 * @author peter
 */
package com.pshirley.pagingmissioncontrol.data;