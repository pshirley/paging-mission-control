package com.pshirley.pagingmissioncontrol.data;

import java.time.ZonedDateTime;

/**
 * Represents an alert of some violation condition occurring. Alerts are
 * immutable value objects.
 * <p>
 * Alerts cannot be directly instantiated. To create an alert use
 * {@link CreateAlert#from(Reading, String)}.
 * </p>
 * 
 * @author peter
 */
public class Alert {
	private final Integer satelliteId;
	private final String component;
	private final String severity;
	private final ZonedDateTime timestamp;

	/**
	 * Creates an alert from the given details. All of the arguments to this
	 * constructor must be non-null.
	 * 
	 * @param satelliteId the ID of the satellite from which this reading was taken
	 * @param component   the component from which this reading was taken
	 * @param severity    the severity of this alert
	 * @param timestamp   the timestamp for this alert
	 */
	// Package-protected so that CreateAlert.from() can use this constructor
	Alert(Integer satelliteId, String component, String severity, ZonedDateTime timestamp) {
		this.satelliteId = satelliteId;
		this.component = component;
		this.severity = severity;
		this.timestamp = timestamp;
	}

	public Integer getSatelliteId() {
		return satelliteId;
	}

	public String getComponent() {
		return component;
	}

	public String getSeverity() {
		return severity;
	}

	public ZonedDateTime getTimestamp() {
		return timestamp;
	}
}
