package com.pshirley.pagingmissioncontrol.monitoring;

import java.util.Map;
import java.util.Optional;

import com.pshirley.pagingmissioncontrol.data.Alert;
import com.pshirley.pagingmissioncontrol.data.Reading;

/**
 * Checks readings against a satellite's component monitors, generating alerts as
 * needed.
 * <p>
 * This class is not thread-safe, and is not intended to be shared between
 * threads.
 * </p>
 * @author peter
 */
class Satellite {
	private final Map<String, ComponentMonitor> componentMonitors;

	/**
	 * Component monitor to use when no other component monitor is found.
	 */
	// @formatter:off
	private final ComponentMonitor nullComponentMonitor = 
			new ComponentMonitor(reading -> { return false; }, 1, "DUMMY MONITOR");
	// @formatter:on

	/**
	 * Creates a Satellite using the given component monitors.
	 * 
	 * @param componentMonitors a map from component name as it appears in a Reading
	 *                          (e.g. "BATT" or "TSTAT") to a monitor for that
	 *                          component. This parameter must not be null.
	 */
	public Satellite(Map<String, ComponentMonitor> componentMonitors) {
		this.componentMonitors = componentMonitors;
	}

	/**
	 * Ask the monitor for the component specified in reading to check the reading
	 * and generate an alert if necessary.
	 * 
	 * @param reading the reading to ask the component's monitor to check. This parameter must not be null.
	 * 
	 * @return an optional containing the alert if an alert was generated, or an
	 *         empty optional otherwise
	 */
	public Optional<Alert> process(Reading reading) {
		final ComponentMonitor monitor = componentMonitors.getOrDefault(reading.getComponent(), nullComponentMonitor);
		return monitor.checkReading(reading);
	}
}
