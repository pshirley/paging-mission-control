package com.pshirley.pagingmissioncontrol.monitoring;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.Queue;

import com.pshirley.pagingmissioncontrol.data.Reading;

/**
 * A container for Readings that ensures all of the readings in the container
 * fall within a given time interval. By default this class ensures that the
 * readings it contains are within five minutes of each other. As new readings
 * are added, older readings are removed to keep all of the readings in this
 * AlertInterval within the time interval.
 * <p>
 * This class is not thread-safe, and is not intended to be shared between
 * threads.
 * </p>
 * 
 * @author peter
 */
class AlertInterval {
	private static final long DEFAULT_INTERVAL_LENGTH = 5;
	private static final ChronoUnit DEFAULT_INTERVAL_UNITS = ChronoUnit.MINUTES;

	private final Queue<Reading> readings;

	// These determine the time window this queue allows.
	private final long intervalLength;
	private final ChronoUnit intervalUnits;

	// This determines the threshold at which alerts should be generated
	private final Integer alertThreshold;

	/**
	 * Create a new AlertInterval with the default time interval (five minutes).
	 * 
	 * @param alertThreshold the number of readings in this alert interval at which
	 *                       alerts should be generated. This parameter must not be
	 *                       null.
	 */
	public AlertInterval(Integer alertThreshold) {
		this(DEFAULT_INTERVAL_LENGTH, DEFAULT_INTERVAL_UNITS, alertThreshold);
	}

	/**
	 * Create a new AlertInterval with the specified time interval. All of the arguments to this constructor must be non-null.
	 * 
	 * @param intervalLength the length of the time interval in intervalUnits units
	 * @param intervalUnits  the units used for intervalLength
	 * @param alertThreshold the number of readings in this alert interval at which
	 *                       alerts should be generated.
	 */
	public AlertInterval(long intervalLength, ChronoUnit intervalUnits, Integer alertThreshold) {
		this.readings = new LinkedList<Reading>();
		this.intervalLength = intervalLength;
		this.intervalUnits = intervalUnits;
		this.alertThreshold = alertThreshold;
	}

	/**
	 * Should an alert be generated? Checks the readings within this alert interval
	 * and returns true if an alert should be generated.
	 * 
	 * @return true if an alert should be generated, false otherwise
	 */
	public boolean shouldGenerateAlert() {
		return readings.size() >= alertThreshold;
	}

	/**
	 * Add a new Reading to this AlertInterval. If necessary this method will remove
	 * any Readings whose timestamps are far enough before the new reading's
	 * timestamp that they no longer fit within this AlertInterval's time interval.
	 * 
	 * @param reading the Reading to add. This parameter must not be null.
	 * 
	 * @return true if the Reading was added successfully
	 */
	public boolean add(Reading reading) {
		for (Reading head = readings.peek(); outsideIntervalLimit(head, reading); head = readings.peek()) {
			readings.remove();
		}
		return readings.add(reading);
	}

	/**
	 * Get the earliest Reading in this AlertInterval.
	 * 
	 * @return the earliest Reading in this AlertInterval
	 */
	public Reading earliestReading() {
		return readings.element();
	}

	/**
	 * Is the first Reading outside of this AlertInterval's time interval when
	 * compared with the second Reading?
	 * 
	 * @param first  the first Reading
	 * @param second the second Reading
	 * 
	 * @return true if the timestamp of the first Reading is before the timestamp of
	 *         the second Reading minus this AlertInterval's time interval, false
	 *         otherwise
	 */
	// Package-protected for ease of unit testing
	boolean outsideIntervalLimit(Reading first, Reading second) {
		if (first == null || second == null) {
			return false;
		}
		final ZonedDateTime firstTimestamp = first.getTimestamp();
		final ZonedDateTime secondTimestamp = second.getTimestamp();

		if (firstTimestamp.isBefore(secondTimestamp.minus(intervalLength, intervalUnits))) {
			return true;
		}

		return false;
	}

	/**
	 * Return the number of readings within this AlertInterval's time interval.
	 * 
	 * @return the number of readings within this AlertInterval's time interval
	 */
	// Package-protected to help with testing
	int count() {
		return readings.size();
	}
}
