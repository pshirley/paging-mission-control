/**
 * This package contains the processing logic for monitoring a stream of telemetry readings.
 * @author peter
 *
 */
package com.pshirley.pagingmissioncontrol.monitoring;