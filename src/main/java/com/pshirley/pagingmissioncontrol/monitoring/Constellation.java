package com.pshirley.pagingmissioncontrol.monitoring;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import com.pshirley.pagingmissioncontrol.data.Alert;
import com.pshirley.pagingmissioncontrol.data.Reading;
import com.pshirley.pagingmissioncontrol.input.ReadingSource;

/**
 * Processes telemetry readings for a set of satellites to generate a list of
 * alerts.
 * <p>
 * This class is not thread-safe, and is not intended to be shared between
 * threads.
 * </p>
 * @author peter
 */
public class Constellation {
	private final Map<Integer, Satellite> satellites = new HashMap<Integer, Satellite>();
	private final List<Alert> alerts = new ArrayList<Alert>();

	private final Supplier<Map<String, ComponentMonitor>> monitorFactory;

	/**
	 * Creates a constellation of satellites with component monitors created by the
	 * given factory.
	 * 
	 * @param monitorFactory a supplier of a map from component name as it appears
	 *                       in a Reading (e.g. "BATT" or "TSTAT") to a
	 *                       ComponentMonitor for that component. This parameter must not be null.
	 */
	public Constellation(Supplier<Map<String, ComponentMonitor>> monitorFactory) {
		this.monitorFactory = monitorFactory;
	}

	/**
	 * Process telemetry readings from the given ReadingSource.
	 * 
	 * @param readings the source for telemetry readings. This parameter must not be null.
	 * 
	 * @return an immutable list of Alerts generated while processing readings
	 */
	public List<Alert> processReadings(ReadingSource readings) {
		for (Reading reading : readings) {
			final Satellite satellite = satellites.computeIfAbsent(reading.getSatelliteId(),
					id -> new Satellite(monitorFactory.get()));

			satellite.process(reading).ifPresent(alert -> alerts.add(alert));
		}

		return Collections.unmodifiableList(alerts);
	}
}
