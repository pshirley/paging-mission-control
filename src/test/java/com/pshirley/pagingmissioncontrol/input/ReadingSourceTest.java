package com.pshirley.pagingmissioncontrol.input;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.StringReader;
import java.time.Month;
import java.time.ZonedDateTime;
import java.util.Iterator;

import org.junit.jupiter.api.Test;

import com.pshirley.pagingmissioncontrol.data.Reading;

public class ReadingSourceTest {
	@Test
	public void testSingleRecordSourceWithDefaultParser() throws IOException {
		final ReadingSource source = ReadingSource
				.from(new StringReader("20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT"));
		final Iterator<Reading> iterator = source.iterator();
		final Reading reading = iterator.next();
		assertNotNull(reading, "First reading should not be null");

		final ZonedDateTime timestamp = reading.getTimestamp();
		assertEquals(2018, timestamp.getYear());
		assertEquals(Month.JANUARY, timestamp.getMonth());
		assertEquals(1, timestamp.getDayOfMonth());
		assertEquals(23, timestamp.getHour());
		assertEquals(1, timestamp.getMinute());
		assertEquals(5, timestamp.getSecond());
		assertEquals(1000000, timestamp.getNano());

		assertEquals(1001, reading.getSatelliteId());
		assertEquals(101, reading.getRedHighLimit());
		assertEquals(98, reading.getYellowHighLimit());
		assertEquals(25, reading.getYellowLowLimit());
		assertEquals(20, reading.getRedLowLimit());
		assertEquals(99.9, reading.getRawValue());
		assertEquals("TSTAT", reading.getComponent());

		assertFalse(iterator.hasNext(), "Iterator should be empty after final reading");
	}

	@Test
	public void testIteratorForEmptySource() {
		final ReadingSource source = ReadingSource.from(new StringReader(""));
		Iterator<Reading> iterator = source.iterator();
		assertFalse(iterator.hasNext());
	}

	@Test
	public void testCallingNextOnEmptySourceIteratorThrowsException() {
		final ReadingSource source = ReadingSource.from(new StringReader(""));
		Iterator<Reading> iterator = source.iterator();
		assertThrows(java.util.NoSuchElementException.class, () -> {
			iterator.next();
		});
	}

	@Test
	public void testIteratorWithSingleRecordSource() throws IOException {
		final ReadingSource source = ReadingSource
				.from(new StringReader("20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT"));
		final Iterator<Reading> iterator = source.iterator();
		assertTrue(iterator.hasNext(), "Iterator should have next record before iterating");
		final Reading reading = iterator.next();
		assertNotNull(reading, "Reading should not be null");
		assertFalse(iterator.hasNext(), "Iterator should have no more records after iterating");
	}
}
