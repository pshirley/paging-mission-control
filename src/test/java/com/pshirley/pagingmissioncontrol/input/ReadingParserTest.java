package com.pshirley.pagingmissioncontrol.input;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.Month;
import java.time.ZonedDateTime;

import org.junit.jupiter.api.Test;

import com.pshirley.pagingmissioncontrol.data.Reading;

public class ReadingParserTest {
	@Test
	public void testParserForSampleInput() {
		final String SAMPLE_INPUT = "20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT";
		final Reading reading = ReadingParser.readingFrom(SAMPLE_INPUT);

		assertNotNull(reading, "Reading should not be null");

		final ZonedDateTime timestamp = reading.getTimestamp();
		assertEquals(2018, timestamp.getYear());
		assertEquals(Month.JANUARY, timestamp.getMonth());
		assertEquals(1, timestamp.getDayOfMonth());
		assertEquals(23, timestamp.getHour());
		assertEquals(1, timestamp.getMinute());
		assertEquals(5, timestamp.getSecond());
		assertEquals(1000000, timestamp.getNano());
		assertEquals("Z", timestamp.getZone().getId());

		assertEquals(1001, reading.getSatelliteId());
		assertEquals(101, reading.getRedHighLimit());
		assertEquals(98, reading.getYellowHighLimit());
		assertEquals(25, reading.getYellowLowLimit());
		assertEquals(20, reading.getRedLowLimit());
		assertEquals(99.9, reading.getRawValue());
		assertEquals("TSTAT", reading.getComponent());
	}
}
