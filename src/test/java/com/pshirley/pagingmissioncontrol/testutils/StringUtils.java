package com.pshirley.pagingmissioncontrol.testutils;

import java.io.StringReader;

/**
 * Test utilities for dealing with strings.
 * 
 * @author peter
 *
 */
public class StringUtils {
	private static final String NEWLINE = "\n";
	
	/**
	 * Create a multiline string built by separating the given lines with newline characters.
	 *  
	 * @param lines the lines to combine into a multiline string
	 * 
	 * @return the lines combined into a multiline string
	 */
	public static String multilineString(String... lines) {
		return String.join(NEWLINE, lines);
	}
	
	/**
	 * Create a string reader with multiple lines, each of which is one line from the given lines.
	 * 
	 * @param lines lines to read using the multiline string reader
	 * 
	 * @return a string reader wrapping a multiline string built from lines
	 */
	public static StringReader multilineReader(String... lines) {
		return new StringReader(multilineString(lines));
	}
}
