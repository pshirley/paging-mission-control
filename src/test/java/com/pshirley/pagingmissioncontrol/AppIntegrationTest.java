package com.pshirley.pagingmissioncontrol;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

/**
 * Integration tests for application. Relies on test files in
 * src/test/resources.
 * 
 * @author peter
 *
 */
public class AppIntegrationTest {
	@Test
	public void testAppWithChallengeSampleData() throws Exception {
		final PrintStream originalStdout = System.out;
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		System.setOut(new PrintStream(baos));

		App.main("src/test/resources/sampledata1.txt");

		System.setOut(originalStdout);

		final JSONArray jsonOutput = new JSONArray(baos.toString());
		assertEquals(2, jsonOutput.length(), "Output should have two alerts");
		checkAlert(jsonOutput.getJSONObject(0), "First alert", 1000, "TSTAT", "RED HIGH", "2018-01-01T23:01:38.001Z");
		checkAlert(jsonOutput.getJSONObject(1), "Second alert", 1000, "BATT", "RED LOW", "2018-01-01T23:01:09.521Z");
	}

	private void checkAlert(final JSONObject alert1, String alertName, Integer satelliteId, String component,
			String severity, String timestamp) {
		assertEquals(satelliteId, alert1.getInt("satelliteId"), alertName + " should be for satellite " + satelliteId);
		assertEquals(component, alert1.getString("component"), alertName + " should be for component " + component);
		assertEquals(severity, alert1.getString("severity"), alertName + " should have severity of " + severity);
		assertEquals(timestamp, alert1.getString("timestamp"), alertName + " should have a timestamp of " + timestamp);
	}
}
