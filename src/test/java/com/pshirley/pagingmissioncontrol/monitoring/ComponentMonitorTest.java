package com.pshirley.pagingmissioncontrol.monitoring;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.ZonedDateTime;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import com.pshirley.pagingmissioncontrol.data.Alert;
import com.pshirley.pagingmissioncontrol.data.Reading;
import com.pshirley.pagingmissioncontrol.input.ReadingParser;

public class ComponentMonitorTest {
	@Test
	public void testNoAlertGeneratedWhenNotNeeded() {
		final ComponentMonitor monitor = new ComponentMonitor(reading -> {
			return reading.getRawValue() > reading.getRedHighLimit();
		}, 2, "RED HIGH");

		// The first reading is over the red high limit, but the second reading is not
		final Reading reading1 = ReadingParser.readingFrom("20180101 23:01:05.001|1001|101|98|25|20|101.5|TSTAT");
		final Reading reading2 = ReadingParser.readingFrom("20180101 23:02:05.002|1001|101|98|25|20|99.9|TSTAT");

		assertFalse(monitor.checkReading(reading1).isPresent(), "First reading should not generate an alert");
		assertFalse(monitor.checkReading(reading2).isPresent(), "Second reading should not generate an alert");
	}

	@Test
	public void testAlertIsGeneratedWhenNeeded() {
		final ComponentMonitor monitor = new ComponentMonitor(reading -> {
			return reading.getRawValue() > reading.getRedHighLimit();
		}, 2, "RED HIGH");

		// Both readings are over the red high limit
		final Reading reading1 = ReadingParser.readingFrom("20180101 23:01:05.001|1001|101|98|25|20|101.1|TSTAT");
		final Reading reading2 = ReadingParser.readingFrom("20180101 23:02:05.002|1001|101|98|25|20|101.2|TSTAT");

		assertFalse(monitor.checkReading(reading1).isPresent(), "First reading should not generate an alert");
		final Optional<Alert> optional = monitor.checkReading(reading2); 
		assertTrue(optional.isPresent(), "Second reading should generate an alert");
		final Alert alert = optional.get();
		assertEquals("TSTAT", alert.getComponent());
		assertEquals("RED HIGH", alert.getSeverity());
		assertEquals(1001, alert.getSatelliteId());
		
		ZonedDateTime timestamp = alert.getTimestamp();
		assertEquals(1, timestamp.getMinute());
		assertEquals(1000000, timestamp.getNano());
	}
}
