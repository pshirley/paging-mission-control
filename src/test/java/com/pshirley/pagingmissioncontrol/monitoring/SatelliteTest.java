package com.pshirley.pagingmissioncontrol.monitoring;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

import com.pshirley.pagingmissioncontrol.data.Reading;
import com.pshirley.pagingmissioncontrol.input.ReadingParser;

public class SatelliteTest {
	@Test
	public void testNoAlertsWhenNoMonitors() {
		final Satellite satellite = new Satellite(new HashMap<String, ComponentMonitor> ());
		final Reading reading = ReadingParser.readingFrom("20180101 23:01:05.001|1001|101|98|25|20|101.5|TSTAT");
		assertFalse(satellite.process(reading).isPresent(), "Satellite with no monitors should not produce alerts");
	}
	
	@Test
	public void testNoAlertWhenOutOfRangeReadingsBelowThreshold() {
		final Map<String, ComponentMonitor> monitors = new HashMap<String, ComponentMonitor> ();
		monitors.put("TSTAT", new ComponentMonitor(reading -> {
			return reading.getRawValue() > reading.getRedHighLimit();
		}, 2, "RED HIGH"));
		
		final Satellite satellite = new Satellite(monitors);
		final Reading reading = ReadingParser.readingFrom("20180101 23:01:05.001|1001|101|98|25|20|101.5|TSTAT");
		assertFalse(satellite.process(reading).isPresent(), "Satellite should not produce alert with only one out-of-range reading");
	}
	
	
	@Test
	public void testAlertProducedWhenOutOfRangeReadingsAtThreshold() {
		final Map<String, ComponentMonitor> monitors = new HashMap<String, ComponentMonitor> ();
		monitors.put("TSTAT", new ComponentMonitor(reading -> {
			return reading.getRawValue() > reading.getRedHighLimit();
		}, 2, "RED HIGH"));
		
		final Satellite satellite = new Satellite(monitors);
		final Reading reading1 = ReadingParser.readingFrom("20180101 23:01:05.001|1001|101|98|25|20|101.5|TSTAT");
		assertFalse(satellite.process(reading1).isPresent(), "Satellite should not produce alert with only one out-of-range reading");
		final Reading reading2 = ReadingParser.readingFrom("20180101 23:02:05.001|1001|101|98|25|20|101.5|TSTAT");
		assertTrue(satellite.process(reading2).isPresent(), "Satellite should produce alert with two out-of-range readings");
	}
}
