package com.pshirley.pagingmissioncontrol.monitoring;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import com.pshirley.pagingmissioncontrol.data.Reading;
import com.pshirley.pagingmissioncontrol.input.ReadingParser;

public class AlertIntervalTest {
	@Test
	public void testTwoReadingsOverFiveMinutesApart() {
		final Reading reading1 = parseReading("20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT");
		final Reading reading2 = parseReading("20180101 23:06:05.002|1001|101|98|25|20|99.9|TSTAT");

		AlertInterval interval = new AlertInterval(2);
		interval.add(reading1);
		interval.add(reading2);

		assertEquals(1, interval.count());
		assertFalse(interval.shouldGenerateAlert());
	}

	@Test
	public void testTwoReadingsUnderFiveMinutesApart() {
		final Reading reading1 = parseReading("20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT");
		final Reading reading2 = parseReading("20180101 23:06:05.000|1001|101|98|25|20|99.9|TSTAT");

		AlertInterval interval = new AlertInterval(3);
		interval.add(reading1);
		interval.add(reading2);

		assertEquals(2, interval.count());
		assertFalse(interval.shouldGenerateAlert());
	}

	@Test
	public void testThreeReadingsLastOneMuchLater() {
		final Reading reading1 = parseReading("20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT");
		final Reading reading2 = parseReading("20180101 23:06:05.000|1001|101|98|25|20|99.9|TSTAT");
		final Reading muchLaterReading = parseReading("20180101 23:17:05.000|1001|101|98|25|20|99.9|TSTAT");

		AlertInterval interval = new AlertInterval(3);
		interval.add(reading1);
		interval.add(reading2);
		assertEquals(2, interval.count());
		assertFalse(interval.shouldGenerateAlert());

		interval.add(muchLaterReading);
		// Adding much later reading should have removed any readings outside of the
		// five minute window
		assertEquals(1, interval.count());
	}

	@Test
	public void testOutsideIntervalLimitWhenTrue() {
		final Reading reading1 = parseReading("20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT");
		final Reading reading2 = parseReading("20180101 23:06:05.002|1001|101|98|25|20|99.9|TSTAT");

		AlertInterval interval = new AlertInterval(3);
		assertTrue(interval.outsideIntervalLimit(reading1, reading2));
	}

	@Test
	public void testOutsideIntervalLimitWhenFalse() {
		final Reading reading1 = parseReading("20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT");
		final Reading reading2 = parseReading("20180101 23:06:05.000|1001|101|98|25|20|99.9|TSTAT");

		AlertInterval interval = new AlertInterval(3);
		assertFalse(interval.outsideIntervalLimit(reading1, reading2));
	}

	@Test
	public void testShouldGenerateAlertWhenOverThreshold() {
		AlertInterval interval = new AlertInterval(2);
		interval.add(parseReading("20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT"));
		interval.add(parseReading("20180101 23:06:05.000|1001|101|98|25|20|99.9|TSTAT"));
		assertTrue(interval.shouldGenerateAlert());
	}

	@Test
	public void shouldNotGenerateAlertWhenUnderThreshold() {
		AlertInterval interval = new AlertInterval(2);
		interval.add(parseReading("20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT"));
		assertFalse(interval.shouldGenerateAlert());
	}

	private Reading parseReading(final String record) {
		return ReadingParser.readingFrom(record);
	}
}
