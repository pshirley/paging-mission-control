package com.pshirley.pagingmissioncontrol.monitoring;

import static com.pshirley.pagingmissioncontrol.testutils.StringUtils.multilineReader;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.pshirley.pagingmissioncontrol.data.Alert;
import com.pshirley.pagingmissioncontrol.input.ReadingSource;

/**
 * @author peter
 *
 */
public class ConstellationTest {
	private Constellation constellation;
	private Supplier<Map<String, ComponentMonitor>> monitorFactory = new Supplier<Map<String, ComponentMonitor>>() {
		public Map<String, ComponentMonitor> get() {
			final Map<String, ComponentMonitor> componentMonitors = new HashMap<String, ComponentMonitor>();

			// @formatter:off
			componentMonitors.put("TSTAT", 
					new ComponentMonitor(reading -> { return reading.getRawValue() > reading.getRedHighLimit(); }, 
							2, "RED HIGH"));
			// @formatter:on

			return componentMonitors;
		}
	};

	@BeforeEach
	public void setUp() {
		constellation = new Constellation(monitorFactory);
	}

	@Test
	public void testNoAlertsWithNoViolationConditions() {
		final ReadingSource source = ReadingSource
				.from(multilineReader("20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT"));
		final List<Alert> alerts = constellation.processReadings(source);

		assertNotNull(alerts);
		assertEquals(0, alerts.size());
	}

	@Test
	public void testOneAlertWithOneViolationCondition() {
		// Reading source has two out-of-range readings, and configured alert threshold
		// is 2, so we should get an alert
		// @formatter:off
		final ReadingSource source = ReadingSource
				.from(multilineReader( 
						"20180101 23:01:05.001|1001|101|98|25|20|101.1|TSTAT",
						"20180101 23:01:06.001|1001|101|98|25|20|101.2|TSTAT"));
		// @formatter:on
		final List<Alert> alerts = constellation.processReadings(source);

		assertNotNull(alerts);
		assertEquals(1, alerts.size());
	}

	@Test
	public void testNoAlertsWithOnlyOneOutOfRangeReading() {
		// @formatter:off
		final ReadingSource source = ReadingSource
				.from(multilineReader(
						"20180101 23:01:05.001|1001|101|98|25|20|99.3|TSTAT",
						"20180101 23:01:06.001|1001|101|98|25|20|101.2|TSTAT"));
		// @formatter:on
		final List<Alert> alerts = constellation.processReadings(source);

		assertNotNull(alerts);
		assertEquals(0, alerts.size());
	}
}
