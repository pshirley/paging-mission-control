# Analyze Satellite Telemetry
## About this application
This application analyzes satellite telemetry data and generates alerts if certain conditions are detected. It takes as input a file containing telemetry data, and outputs a JSON array containing alert objects. Details can be found in REQUIREMENTS.md.

## Building this application
This application requires Java to run and Maven to build. To build this application, use:

    mvn clean package
 
This will create an executable JAR file in the "target" directory called paging-mission-control-VERSION.jar, where VERSION is 0.0.1-SNAPSHOT at this writing.

## Running this application
To run this application, use:

    java -jar target/paging-mission-control-VERSION.jar <input-filename>
 
Where &lt;input-filename&gt; is replaced with the path to the file containing satellite telemetry data. The application will read data from the input file and print the resulting JSON data on stdout. Any errors will be printed on stderr.

# Technical Details

## Overview
This application is segregated into four packages:
* the main package (com.pshirley.pagingmissioncontrol) contains the App class, which is responsible for setting up the application, orchestrating the interaction of the other three packages, and printing the alerts.
* The data package (com.pshirley.pagingmissioncontrol.data), which contains value objects used by other parts of the application and a utility class to assist with constructing alerts.
* The input package (com.pshirley.pagingmissioncontrol.input), which handles reading the input data and turning it into Reading value objects.
* The monitoring package (com.pshirley.pagingmissioncontrol.monitoring), which processing Readings and generates Alerts when a violation condition is detected.

Javadoc for this application can be built with:

    mvn javadoc:javadoc
 
## General data flow

At startup, the App class creates a ReadingSource based on the input file and a Constellation of satellites, then passes the ReadingSource to the Constellation for processing. The Constellation returns a list of Alerts, which the App class prints to stdout.

### A note on capitalization
In the rest of this section class names will be Capitalized, while words that refer to concepts rather than classes will not. Plurals become a little tricky with this scheme, but this application does not contain any classes with pluralized names. Thus, a reference to Readings can safely be taken to mean "multiple Reading objects" rather than a class named "Readings".

### Alert and Reading from the data package
The data package provides two value classes, Alert and Reading. A Reading instance represents a single line in the input file. An Alert instance represents a single alert in the output. 

### ReadingSource and the input package
The input package handles transforming lines from the input file into a more convenient representation. ReadingSource provides an Iterator which allows for stepping over the lines in the file one at a time, converting each successive line to a Reading object. This provides a convenient mechanism for the rest of the application to process readings without worrying about the format or medium used to specify each reading. ReadingSource uses ReadingParser from the input package to parse individual lines from the input file.

### Constellation and other classes in the monitoring package
The monitoring package handles the actual data analysis and alert generation. It contains four classes: Constellation and ComponentMonitor are public, while Satellite and AlertInterval are package visible. This section describes the interaction of these classes.

Constellations are responsible for managing data for a set of satellites and generating a list of alerts when readings from components on those satellites are out-of-range for a set time period. To detect out-of-range readings, the Constellation uses ComponentMonitors created by a factory object which App passes to the Constellation's constructor. This factory creates a Satellite object and a collection of ComponentMonitors for each satellite ID that appears in the input. The application assumes that if a satellite ID appears in the input file it is a valid ID, so when an unknown satellite ID is encountered a new Satellite is constructed and added to the collection of known satellites.

The Constellation uses the Iterator provided by ReadingSource to step through the Readings. For each Reading, it gets the satellite ID from the Reading, obtains a Satellite object for that ID, and asks the Satellite to process the Reading. When the satellite processes the Reading it get the component name from the Reading, then fetches the ComponentMonitor for that component from the collection of ComponentMonitors for the Satellite. The Satellite then asks the ComponentMonitor to check the Reading and generate an Alert if appropriate.

The ComponentMonitor checks the Reading to see if the raw value for the Reading is outside the Reading's value for the limit the ComponentMonitor watches. For ComponentMonitors watching the BATT component the raw value is checked against the red-low limit in the Reading, while for ComponentMonitors watching the TSTAT component the raw value is checked against the red-high limit. 

If the raw value is out of range, the Reading is added to a smart collection of out-of-range readings (an AlertInterval). This collection checks the timestamp of the new reading and removes any readings that are outside the time interval allowed by the collection (five minutes by default). If after removing these Readings there are still enough out-of-range Readings in the collection to satisfy the violation condition then an Alert is returned in an Optional back to the Constellation, which adds it to its list of Alerts.

When the ReadingSource has been exhausted the Constellation returns a list of Alerts to App, which prints the list on the console as a JSON array of objects.

# Outstanding Issues
* While the description in the previous section lays out the interaction between classes in general terms, it omits a few classes and is somewhat tiresome to read. In some organizations a UML diagram might be useful to depict the classes and the relationships between them. No such diagram exists.
* I made a host of assumptions about the requirements, most of which I would ordinarily have cleared up during an initial discussion about requirements. I've documented those assumptions in ASSUMPTIONS.md. It is entirely possible that some or all of these assumptions are incorrect.
* I have not characterized the performance of this application, other than that it's fast enough on a tiny set of input data. There are a number of things that could be done to make it faster if necessary, such as addressing some thread-safety issues and pushing processing to multiple threads (or to multiple machines, if that were warranted). 
* I believe the application is reasonably cautious with memory use, but I have not done a thorough analysis. I am a bit concerned that creating new Readings for every input record will result in a lot of garbage collection (see note on performance), especially since one hopes that most input records would not contain out-of-range values. Fixing that might have made things considerably less readable, however. 
* While working on this project I have tried to strike a balance between overengineering and dispensing with design entirely. I may have gone too far in either or both directions. In particular, I resisted pushing checks against readings into little strategies as long as I could, fearing that was overcomplicating things -- but in the end I did it anyway. On a real project with more than one participant I would have welcomed the chance to discuss this aspect of the design.
* As implied above, I haven't used any test data other than that supplied with the challenge. In future I may play with generating large (million line) files and seeing how this application handles them, how the applicaton's memory usage changes over time, and in particular how long it takes to complete.

# References
The requirements for this project can be found in REQUIREMENTS.md.
A lengthly list of assumptions made during implementation can be found in ASSUMPTIONS.md.